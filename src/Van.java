import org.w3c.dom.ls.LSOutput;

import java.util.ArrayList;

public class Van {
    private double maxLoadWeightCargo;
    private ArrayList<Coffee> coffeeList = new ArrayList<>();
    private double loadedVan = 0;

    public Van(double maxLoadWeightCargo) {
        this.maxLoadWeightCargo = maxLoadWeightCargo;
    }

    public double getMaxLoadWeightCargo() {
        return maxLoadWeightCargo;
    }

    public ArrayList<Coffee> getCoffeeList() {
        return coffeeList;
    }

    public double getLoadedVan() {
        return loadedVan;
    }

    public boolean addCoffee(Coffee coffee) {
        boolean answer = false;
        if (!checkPossibilityAddCoffeeInVan(coffee.getWeight())) {
            coffeeList.add(coffee);
            loadedVan += coffee.getWeight();
            answer = true;
        }
        return answer;
    }

    public boolean checkPossibilityAddCoffeeInVan(double weight) {
        return loadedVan + weight > maxLoadWeightCargo || weight <= 0;
    }

    public String giveInfo() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Фургон: ").append(loadedVan).append("г/").append(maxLoadWeightCargo).append("г\n").append("\nСписок кофе в фургоне:\n");
        for (int i = 0; i < coffeeList.size(); i++) {
            stringBuilder.append("\n[").append(i + 1).append("]\n").append(coffeeList.get(i).giveInfo()).append("\n");
        }
        return stringBuilder.toString();
    }

    public String searchCoffee(String sortCoffee, String physicalState, double weight, double price) {
        System.out.println(sortCoffee);
        ArrayList<Coffee> newArrayList;
        newArrayList = search(coffeeList, sortCoffee);
        newArrayList = search(newArrayList, physicalState);
        newArrayList = search(newArrayList, weight, price);

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Найдено ").append(newArrayList.size()).append(" совпадения(й, е):");
        for (int i = 0; i < newArrayList.size(); i++) {
            stringBuilder.append("\n\n[").append(i + 1).append("]\n").append(newArrayList.get(i).giveInfo()).append("\n");
        }
        return stringBuilder.toString();
    }

    private ArrayList<Coffee> search(ArrayList<Coffee> arrayList, String str) {
        ArrayList<Coffee> newArrayList = new ArrayList<>();
        if (!str.equals("-")) {
            for (Coffee coffee : arrayList) {
                System.out.println(coffee.getSortCoffee());
                if (coffee.getSortCoffee().equals(str) || coffee.getPhysicalState().equals(str)) {
                    newArrayList.add(coffee);
                }
            }
        }

        return newArrayList;
    }

    private ArrayList<Coffee> search(ArrayList<Coffee> arrayList, double weight, double price) {
        ArrayList<Coffee> newArrayList = new ArrayList<>();
        if (weight != 0) {
            for (Coffee coffee : arrayList) {
                if (coffee.getWeight() <= weight) {
                    newArrayList.add(coffee);
                }
            }
        } else if (price != 0) {
            for (Coffee coffee : arrayList) {
                if (coffee.getPrice() <= price) {
                    newArrayList.add(coffee);
                }
            }
        }

        return newArrayList;
    }
}
