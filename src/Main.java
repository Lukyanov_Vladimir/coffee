import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    private static final String URL = "src\\file\\coffee.txt";

    public static void main(String[] args) {
        ArrayList<Coffee> coffeeList = ReadFile.read(URL);

        double money = 1000.435;
        Van van = new Van(1000.435);

        CoffeeLoader cl = new CoffeeLoader(van, money, coffeeList);
        cl.loadCoffee();


        System.out.println(cl.giveInfo());

        Scanner sc = new Scanner(System.in);

        System.out.println("Хотите найти конкретное кофе?" +
                "\n1.Да" +
                "\n2.Нет");
        if (sc.nextInt() == 1) {
            System.out.println("Введите сорт кофе (если не знаете, то введите \"-\")");
            String sortCoffee = sc.next();
            System.out.println("Введите физическое сосотояние кофе (если не знаете, то введите \"-\")");
            String physicalState = sc.next();
            System.out.println("Введите вес кофе (если не знаете, то введите \"0\")");
            double weight = sc.nextDouble();
            System.out.println("Введите цену кофе (если не знаете, то введите \"0\")");
            double price = sc.nextDouble();
            System.out.println(van.searchCoffee(sortCoffee, physicalState, weight, price));
        }
    }
}
