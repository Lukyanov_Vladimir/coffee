import java.util.ArrayList;

public class CoffeeLoader {
    private Van van;
    private double maxMoney;
    private double currentBalanceSheet = 0;
    private ArrayList<Coffee> coffeeList;

    public CoffeeLoader(Van van, double maxMoney, ArrayList<Coffee> coffeeList) {
        this.van = van;
        this.maxMoney = maxMoney;
        this.coffeeList = coffeeList;
    }

    public void loadCoffee() {
        while (currentBalanceSheet < maxMoney && van.getLoadedVan() < van.getMaxLoadWeightCargo()) {
            Coffee coffee = coffeeList.get(randomCoffee(coffeeList.size()));

            while (!addCoffee(coffee)) {
                if (checkCurrentBalanceSheet(coffee.getPrice())) {
                    double lackPrice = maxMoney - currentBalanceSheet;
                    double newWeight = recalculateWeightAccordingPrice(lackPrice, coffee.getPrice(), coffee.getWeight());
                    coffee = createCoffeeObjectWithRecalculatedPriceOrWeight(coffee, newWeight, lackPrice);

                } else if (van.checkPossibilityAddCoffeeInVan(coffee.getWeight())) {
                    double lackWeight = van.getMaxLoadWeightCargo() - van.getLoadedVan();
                    double newPrice = recalculatePriceAccordingWeight(lackWeight, coffee.getWeight(), coffee.getPrice());
                    coffee = createCoffeeObjectWithRecalculatedPriceOrWeight(coffee, lackWeight, newPrice);
                }
            }
        }
    }

    public String giveInfo() {
        return "Деньги: " + currentBalanceSheet + "руб/" + maxMoney + "руб\n" + van.giveInfo();
    }

    private int randomCoffee(int max) {
        return (int) (Math.random() * max);
    }

    private boolean checkCurrentBalanceSheet(double price) {
        return currentBalanceSheet + price > maxMoney || price <= 0;
    }

    private double recalculateWeightAccordingPrice(double price, double originalPrice, double originalWeight) {
        return price / originalPrice * originalWeight;
    }

    private double recalculatePriceAccordingWeight(double weight, double originalWeight, double originalPrice) {
        return weight * originalPrice / originalWeight;
    }

    private Coffee createCoffeeObjectWithRecalculatedPriceOrWeight(Coffee coffee, double weight, double price) {
        return new Coffee(coffee.getSortCoffee(), coffee.getPhysicalState(), weight, price);
    }

    private void addCurrentBalanceSheet(double price) {
        currentBalanceSheet += price;
    }

    private boolean addCoffee(Coffee coffee) {
        boolean permission = false;
        if (checkPossibilityAdding(coffee)) {
            van.addCoffee(coffee);
            addCurrentBalanceSheet(coffee.getPrice());
            permission = true;
        }
        return permission;
    }

    private boolean checkPossibilityAdding(Coffee coffee) {
        return !checkCurrentBalanceSheet(coffee.getPrice()) && !van.checkPossibilityAddCoffeeInVan(coffee.getWeight());
    }
}
