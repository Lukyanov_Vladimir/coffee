public class Coffee {
    private double weight;
    private String sortCoffee;
    private String physicalState;
    private double price;

    public Coffee(String sortCoffee, String physicalState, double weight, double price) {
        this.sortCoffee = sortCoffee;
        this.physicalState = physicalState;
        this.weight = weight;
        this.price = price;
    }

    public String getSortCoffee() {
        return sortCoffee;
    }

    public String getPhysicalState() {
        return physicalState;
    }

    public double getWeight() {
        return weight;
    }

    public double getPrice() {
        return price;
    }

    public String giveInfo() {
        return "Сорт: " + sortCoffee +
                "\nФизическое состояние: " + physicalState +
                "\nВес: " + weight +
                "г\nЦена: " + price + "руб";
    }


    public boolean equals(Coffee coffee) {
        return this.sortCoffee ==  coffee.getSortCoffee();
    }
}
